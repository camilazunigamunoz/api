const request = require('request');

const endpointCountries = function () {
  this.countriesAll = async (url) => {
    return new Promise((resolve, reject) => {
      request(url, (err, resp, body) => {
        if (err) {
          reject(err);
        }
        if (body.length === 0) {
          reject('Error de conexion');
        }
        resolve(body);
      }
      );
    });
  };
  this.countrieName = async (url) => {
    return new Promise((resolve, reject) => {
      request(url, (err, resp, body) => {
        if (err) {
          reject(err);
        }
        if (!body) {
          reject('Error de conexion');
        }
        if (body === undefined) {
          reject('undefined');
        }
        resolve(body);
      }
    );
    });
  };
};

module.exports = endpointCountries;
