const request = require('request');



const countLang = function () {
    this.languagesCountries = async (url) => {
      return new Promise((resolve, reject) => {
        request(url, (err, resp, body) => {
          if (err) {
            reject(err);
          }
          if (body.length === 0) {
            reject('Error de conexion');
          }
          let contador = 0;
          const dataFormat = JSON.parse(body);
          for (let item in dataFormat){
              contador++; 
          }
          console.log(contador);
          resolve({
              cont: contador});
        });

       });
    }
};

module.exports = countLang;
