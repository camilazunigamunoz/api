const express = require('express');
const bodyParser = require('body-parser');
const compression = require('compression');
const catRoute = require('./app/routes/cat-route');
const countriesRoute = require('./app/controllers/countries-controller');
const langRoute = require('./app/controllers/lang-controller')

const config = require('./app/config');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(compression());


// health check MS
app.get('/api/pet/health/', (req, res) => {
  res.send(`${config.msConfig.name} up and running`);
});

app.use('/api/pet/cat', catRoute);



app.get('/api/v1/info/countries', (req, res) => {
  const newCountries = new countriesRoute();
  newCountries.countriesAll('https://restcountries.eu/rest/v2/all')
  .then((result) => {
    res.status(200).send(result);
  }).catch((err) => {
    res.status(400).send(err);
  });
});


app.get('/api/v1/info/countries/:name', (req, res) => {
  const newCountries = new countriesRoute();
  const country = req.params.name;
  newCountries.countrieName(`https://restcountries.eu/rest/v2/name/${country}`)
  .then((result) => {
    res.send(result);
  }).catch((err) => {
    res.status(400).send(err);
  });
});


app.get('/api/v1/info/lang/:lang', (req, res) => {
  const newLang = new langRoute();
  const lang = req.params.lang
  newLang.languagesCountries(`https://restcountries.eu/rest/v2/lang/${lang}`)
  .then((result) => {
    res.send(result);
  }).catch((err) => {
    res.status(400).send(err);
  });
});

module.exports = app;